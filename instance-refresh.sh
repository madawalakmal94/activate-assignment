#!/bin/bash

aws autoscaling start-instance-refresh --auto-scaling-group-name ac-ass-asg --preferences '{"InstanceWarmup": 60, "MinHealthyPercentage": 50}'
